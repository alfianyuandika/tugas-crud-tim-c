const connection = require("../models/connection");

// Get all transaksi data
const getAll = (req, res) => {
  // Get all query
  let sql = "SELECT * from barang";

  // Run Query
  connection.query(sql, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};

const getOne = (req, res) => {
  // Get One Query
  let sql = `SELECT * from barang WHERE barang.id = ${req.params.id}`;

  // Run Query
  connection.query(sql, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

const create = (req, res) => {
  let sqlCreate = `INSERT INTO barang(id_pemasok, nama, harga) VALUES (${req.body.id_pemasok}, ${req.body.nama}, ${req.body.harga})`;

  // Run Query
  connection.query(sqlCreate, (err, results) => {
    // // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};

const update = (req, res) => {
  let sqlUpdate = `UPDATE barang SET nama = ${req.body.nama}, harga = ${req.body.harga}, id_pemasok = ${req.body.id_pemasok} WHERE id = ${req.params.id};`;
  connection.query(sqlUpdate, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(201).json({
      message: "Success",
      data: results[0],
    });
  });
};

const deleteData = (req, res) => {
  // Delete Query
  let sql = "DELETE FROM transaksi WHERE id = ?";

  // Run Query
  connection.query(sql, [req.params.id], (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
    });
  });
};

module.exports = { getAll, getOne, create, update, deleteData };
