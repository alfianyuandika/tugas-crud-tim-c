// Import database connection
const connection = require("../models/connection");

// Get all pelanggan data
const getAll = (req, res) => {
  // Get all query
  let sql = "SELECT * FROM pelanggan";

  // Run Query
  connection.query(sql, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};

const getOne = (req, res) => {
  // Get One Query
  let sql = `SELECT * FROM pelanggan WHERE id = ${req.params.id}`;

  // Run Query
  connection.query(sql, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

const create = (req, res) => {
  let sqlCreate = `INSERT INTO pelanggan(nama) VALUES (${req.body.nama})`;

  // Run Query
  connection.query(sqlCreate, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(201).json({
      message: "Success",
      data: results,
    });
  });
};

const update = (req, res) => {
  // Get One Query
  let sql = `UPDATE pelanggan SET nama = ${req.body.nama} WHERE id = ${req.params.id};`;

  // Run Query
  connection.query(sql, (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

const deleteData = (req, res) => {
  // Delete Query
  let sql = "DELETE FROM pelanggan WHERE id = ?";

  // Run Query
  connection.query(sql, [req.params.id], (err, results) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    // If no error
    return res.status(200).json({
      message: "Success",
    });
  });
};

module.exports = { getAll, getOne, create, update, deleteData };
