const express = require("express");
const router = express.Router();

// Import controller
const pelangganController = require("../controllers/pelangganController");

// Define routes
router.get("/", pelangganController.getAll);
router.get("/:id", pelangganController.getOne);
router.post("/", pelangganController.create);
router.put("/:id", pelangganController.update);
router.delete("/:id", pelangganController.deleteData);

module.exports = router;
