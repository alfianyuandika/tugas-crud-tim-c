const express = require("express");
const app = express();

// Import Router
const transaksiRoutes = require("./routes/transaksiRoutes");
const pelangganRoutes = require("./routes/pelangganRoutes");
const barangRoutes = require("./routes/barangRoutes");
const pemasokRoutes = require("./routes/pemasokRoutes")

// Use to read req.body
app.use(express.urlencoded({ extended: true }));

// Define routes
app.use("/transaksi", transaksiRoutes);
app.use("/pelanggan", pelangganRoutes);
app.use("/barang", barangRoutes);
app.use("/pemasok", pemasokRoutes);

// Server running
app.listen(3030, () => console.log("Server running on 3030!"));
